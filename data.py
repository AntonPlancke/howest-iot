import requests
import smbus2
import bme280
import looper

port = 1
address = 0x76
bus = smbus2.SMBus(port)

calibration_params = bme280.load_calibration_params(bus, address)

push_url = "https://competent-sammet-916903.netlify.com/.netlify/functions/data"

def get_data():
    data = bme280.sample(bus, address, calibration_params)
    return {
        'temperature': data.temperature,
        'humidity': data.humidity,
        'pressure': data.pressure
    }

def push_data(data):
    print(requests.post(push_url, json=data).text)

def update():
    push_data(get_data())

def start_data_sender():
    looper.Interval(update, "interval_data").start()

if __name__ == "__main__":
    print get_data()
