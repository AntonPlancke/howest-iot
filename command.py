from config import config


def create_setter(name, parser=int):
	"""
	command format:
	command_name value
	"""
	def setter(options):
		config[name] = parser(options[0])
	return setter


def create_min_max_setter(name, parser=int):
	"""
	command format:
	command_name <min|max> value
	"""
	def setter(options):
		min_max = options[0]
		config[name][min_max] = parser(options[1])
	return setter


def get_help(options):
	return commands


def set_device_name(options):
	# todo
	return "unsupported"


commands = {
	"get_help": get_help,
	"set_interval_checker": create_setter("interval_checker"),
	"set_interval_data": create_setter("interval_data"),
	"set_temperature": create_min_max_setter("temperature"),
	"set_humidity": create_min_max_setter("humidity"),
	"set_pressure": create_min_max_setter("pressure"),
	"set_device_name": set_device_name
}


def execute_command(command):
	"""
	command format:
	`comandName [options...]`
	"""
	parts = command.split(" ")
	command = parts[0]
	options = parts[1:]

	try:
		return str(commands[command](options))
	except:
		return str(commands.keys())


def main():
	while True:
		print("Current config: ")
		print(config)

		command = input("$ ")
		if command in {"exit", "quit", "q"}:
			break
		print(execute_command(command))


if __name__ == "__main__":
	main()
