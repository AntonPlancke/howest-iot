from config import config
from time import sleep
from threading import Thread

class Interval(Thread):
	def __init__(self, function, interval):
		Thread.__init__(self)
		self.function = function
		self.interval = interval

	def run(self):
		while True:
			self.function()
			sleep(config[self.interval])

class Loop(Thread):
	def __init__(self, function):
		Thread.__init__(self)
		self.function = function

	def run(self):
		while True:
			self.function()
