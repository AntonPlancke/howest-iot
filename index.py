import os, looper, check, server
from config import config

def on_check():
    print("Check was True")
    os.system("echo iot123 | sudo -S shutdown -r now")

# Listen for cloud event
check.start_checker(on_check)

# Send data to netlify
import data
data.start_data_sender()

# Listen for bluetooth connections
looper.Loop(server.open_connection).start()
