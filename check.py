import requests
import location
import looper

checker_url = "https://competent-sammet-916903.netlify.com/.netlify/functions/weatherChecker"

def get_checker_id(lon, lat):
	return requests.post(checker_url, json={
		"lon": lon,
		"lat": lat
	}).text

def check(checker_id):
	params = {"id": checker_id}
	return requests.get(checker_url, params=params).text == 'true'

def start_checker(action):
	longitude, latitude = location.get_location()
	checker_id = get_checker_id(longitude, latitude)

	def try_action():
		if check(checker_id):
			action()
		else:
			print("No check")

	looper.Interval(try_action, "interval_checker").start()

def test_action():
	print("check")

if __name__ == "__main__":
	start_checker(test_action)
