config = {
    "interval_checker": 2,
    "interval_data": 10,
    "temperature": {
        "min": 260,
        "max": 310
    },
    "humidity": {
        "min": 0,
        "max": 100,
    },
    "pressure": {
        "min": 0,
        "max": 2
    }
}
