import requests

url = "https://freegeoip.live/json/"

def get_location():
    data = requests.get(url).json()
    return data['longitude'], data['latitude']